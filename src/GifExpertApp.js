import { useState } from "react";
import { AddCategory } from "./components/AddCategory";
import { GifGrid } from "./components/GifGrid";

const GifExpertApp = () => {
  
const [catergories, setCatergories] = useState(['One Punch']);

return (
  <>
    <h2>GifExpertApp</h2>
    <AddCategory setCategories={setCatergories}/>
    <hr />

    <ol>
      {
        catergories.map( catergory => 
          <GifGrid 
            key={catergory} 
            category={catergory}/>)
      }
    </ol>
  </>
)

}

export default GifExpertApp;